
REM 1.First we create an certificate
makecert.exe ^
-n "CN=CARoot" ^
-r ^
-pe ^
-a sha512 ^
-len 4096 ^
-cy authority ^
-sv CARoot.pvk ^
CARoot.cer

REM 2. We copie the public key and private key information contained in the .pvk and .cer file to 
REM a Personal Information Exchange file.
pvk2pfx.exe ^
-pvk CARoot.pvk ^
-spc CARoot.cer ^
-pfx CARoot.pfx ^
-po Test123


