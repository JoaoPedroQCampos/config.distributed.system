﻿using System;

namespace Confluent_Producer
{
    public class Event1
    {
        public Guid Id { get; set; }

        public DateTime Timestamp { get; set; }

        public string Msg { get; set; }
    }
}