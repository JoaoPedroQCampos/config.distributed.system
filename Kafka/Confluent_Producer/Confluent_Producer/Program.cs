﻿using Confluent.Kafka;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Confluent_Producer
{

    class Program
    {
        public static async Task Main(string[] args)
        {
            var config = new ProducerConfig
            {
                BootstrapServers = "192.168.0.20:9092,192.168.0.20:9091"
            };

            var msg = new Event1
            {
                Id = Guid.NewGuid(),
                Timestamp = DateTime.Now,
                Msg = "Parabens Joao 2, conseguiste..."
            };

            var msgToKafka = JsonConvert.SerializeObject(msg);

            using (var p = new ProducerBuilder<Null, byte[]>(config).Build())
            {
                for (int i = 0; i < 20; i++)
                {
                    Thread.Sleep(6000);
                    try
                    {
                        var dr = await p.ProduceAsync("joao_topic", new Message<Null, byte[]> { Value =Encoding.UTF8.GetBytes(msgToKafka)});
                        Console.WriteLine($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
                    }
                    catch (ProduceException<Null, string> e)
                    {
                        Console.WriteLine($"Delivery failed: {e.Error.Reason}");
                    } 
                }
            }

            Console.ReadLine();
        }
    }
}
