﻿using KafkaNet;
using KafkaNet.Model;
using KafkaNet.Protocol;
using System;
using System.Collections.Generic;

namespace C_Sharp_Producer
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. Create Producer Properties 
            // 1.1 bootstrap.servers 
            // 1.2 Serializacao 
            Uri uri = new Uri("http://localhost:9092");
            var options = new KafkaOptions(uri);
            var router = new BrokerRouter(options);

            // 2. Create the Producer 
            var client = new Producer(router);

            // 3. Create a Producer Record:
            string payload = "Welcome to Kafka!2";
            string topic = "IDGTestTopic";
            Message msg = new Message(payload);

            // 4. Send the Data 
            client.SendMessageAsync(topic, new List<Message> { msg }).Wait();
            Console.ReadLine();
        }
    }
}
