﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ConfiguringIIS.Startup))]
namespace ConfiguringIIS
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
