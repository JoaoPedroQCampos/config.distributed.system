Fazer o deploy de uma Web Forms Application in .NET Framework to IIS : 
	1. Em primeiro devemos ter um site. A não ser que queiramos que a application fique agregada ao Default Site que já se encontrar
	   por default. 
	2. Depois precisamos de criar uma Virtual Directory. Podemos fazê-lo através do VS - ele vai criar por nós o que é necessário no 
	   IIS ou atráves mesmo do IIS.
	3. Falando do processo através do IIS - o processo manual, digamos. 
	     3.1 Add Application 
	        3.1.1 Define Alias, Application Pool and Physical Path.  
		      O Physical Path deve ser o Project Folder Path e não o Solution Folder Path. 
	And that's it: 
	We can them access with a client application like the browser with : 
		http://localhost/[NameOfTheApplication]/[NameOfTheWebService]
~	For example:
		http://localhost/CalculatorWebApplication/CalculatorWebService.asmx

