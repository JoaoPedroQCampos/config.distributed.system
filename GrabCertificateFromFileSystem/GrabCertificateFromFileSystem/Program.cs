﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace GrabCertificateFromFileSystem
{
    class Program
    {
        public static ConsoleKey UserDecision { get; set; }

        static void Main(string[] args)
        {
            Console.WriteLine("Let's play with Http clients and Certificates \n");

        MaisUmaVoltinha:

            Console.WriteLine("Press 0 : To fetch a .pfx file from the File System and turn it into a object \n");
            Console.WriteLine("Press 1 : To fetch a .pfx file from the SO Certifcate Store \n");

            var input = Console.ReadKey();

            switch (input.Key) //Switch on Key enum
            {
                case ConsoleKey.NumPad0:
                case ConsoleKey.D0:
                    GetCertificateFromFileSystem();
                    break;
                case ConsoleKey.NumPad1:
                case ConsoleKey.D1:
                    GetCertificateFromCertificateStore();
                    break;
            }

            CheckIfUserWantsToGoBack();

            if (Program.UserDecision == ConsoleKey.NumPad1 || Program.UserDecision == ConsoleKey.D1)
            {
                goto MaisUmaVoltinha;
            }
        }

        private static void GetCertificateFromCertificateStore()
        {
            // 1. Abrir a loja e fazer um check : 
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

            try
            {
                Console.WriteLine("\t Store is open ? Yes");
                Console.WriteLine("\t Number of certificates of the store : {0}", store.Certificates.Count);
                Console.WriteLine("\t Name of the Store : {0}", store.Name);
                Console.WriteLine("\t Location of the store : {0}", store.Location);
            }
            catch (CryptographicException)
            {
                Console.WriteLine("\t Store is open ? No");
                Console.WriteLine("\t Name of the Store : {0}", store.Name);
                Console.WriteLine("\t Location of the store : {0}", store.Location);
            }

            Console.WriteLine();

            // 2. ir buscar os certificados 
            X509Certificate2Collection certCollection = store.Certificates;

            foreach (var cert in certCollection)
            {
                Console.WriteLine();
                Console.WriteLine("\tCertificate Name : " + cert.Subject);
                Console.WriteLine("\tEmissor : " + cert.Issuer);
                Console.WriteLine("\tPublic key : " + cert.PublicKey);
                Console.WriteLine("\tSerial Number : " + cert.SerialNumber);
            }

            try
            {
                // 3. Encontrar um certificado em especifico :
                // 3.1 Por Serial Number 
                Console.WriteLine("\n Vamos tentar encontrar o meu certificate por Serial Number ");
                var myCert = certCollection.Find(X509FindType.FindBySerialNumber, "507B2DA521D1A19549F99C7A56EAD137", false);
                Console.WriteLine("\t Certificate Name : " + myCert[0].Subject + "\n\t Serial Number : " + myCert[0].SerialNumber);

                // 3.2 Por Name ou "Subject"
                Console.WriteLine("\n Vamos tentar encontrar o certificate do Ipass por Name como eles fazem ");
                var myCert2 = certCollection.Find(X509FindType.FindBySubjectDistinguishedName, "CN=BM.IPASS.RETAIL, OU=Microservice, O=Bank Millennium S.A.", false);
                Console.WriteLine("\t Certificate Name : " + myCert2[0].Subject + "\n\t Serial Number : " + myCert2[0].SerialNumber);
            }
            finally
            {
                store.Close();
            }
        }

        private static void GetCertificateFromFileSystem()
        {
            string certPath = "D:\\Certificates\\Client_UAT_27_02_2020\\client.p12";

            // Load certificate from .pfx file to an obj representation
            X509Certificate cert = new X509Certificate(certPath, "test123");

            string result = cert.ToString();
            Console.WriteLine("\t\n Result : " + result);

            string resultFalse = cert.ToString(false);
            Console.WriteLine("\t\n Result : " + resultFalse);

        }

        #region Program Utils
        public static void CheckIfUserWantsToGoBack()
        {
            Console.WriteLine("Do you want to go back to menu ? \n");
            Console.WriteLine("Press 1 : to go back to menu");
            Console.WriteLine("Press 0 : press 0 to exit \n");

            var input = Console.ReadKey();

            Program.UserDecision = input.Key;
        }

        #endregion

    }
}
